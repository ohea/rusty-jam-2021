#![allow(mixed_script_confusables)]

use bevy::core::FixedTimestep;
use bevy::prelude::*;
use jam_common::map::{Grid, GridTile};
use jam_common::net::{prelude::*, NetStatus};
use std::time::Duration;

pub const SIM_DMILLIS: u64 = 50;
pub const SIM_DT: f64 = SIM_DMILLIS as f64 / 1000.;
pub const SIM_DELTA: Duration = Duration::from_millis(SIM_DMILLIS);
pub const SIM: &'static str = "sim";

mod enemy;
mod energy;
mod game;
mod map;
mod tower;
mod wave;

//fn setup(mut com: Commands) {
//    let map = map::Map::new(map::Map::MAP_WIDTH, map::Map::MAP_HEIGHT, 8);
//}

fn log_info(net: Res<NetStatus>, ts: Res<bevy::core::FixedTimesteps>) {
    if !net.is_changed() {
        return;
    }

    let ts = match ts.get(SIM) {
        Some(t) => t,
        None => return,
    };

    eprint!(
        "\r{} Clients\tTick is off by {:.2}\tNet stats: {:.0}p/s\t({:.0} unique) =\t{:.2}kB/s\t",
        net.peers,
        ts.accumulator(),
        net.packets_per_second,
        net.unique_per_second,
        net.bytes_per_second / 100.,
    )
}

fn main() -> Result<(), anyhow::Error> {
    jam_common::pretty_env_logger::init();

    let (map, grid) = map::Map::new(jam_common::map::WIDTH, jam_common::map::HEIGHT, 8);
    App::build()
        .add_plugins(MinimalPlugins)
        .add_stage_after(
            CoreStage::Update,
            SIM,
            SystemStage::parallel().with_run_criteria(FixedTimestep::step(SIM_DT).with_label(SIM)),
        )
        .add_system(log_info.system())
        // Modules
        .add_plugin(enemy::EnemyPlugin)
        .add_plugin(tower::TowerPlugin)
        .add_plugin(energy::EnergyPlugin)
        .add_plugin(wave::WavePlugin)
        .add_plugin(game::GamePlugin)
        // Network
        .enable_send(ResHook::<Grid<GridTile>>::RELIABLE)
        //.add_startup_system(setup.system())
        .insert_resource(map)
        .insert_resource(grid)
        .add_plugin(NetPlugin::start(("0.0.0.0", jam_common::DEFAULT_PORT))?)
        .run();

    Ok(())
}
