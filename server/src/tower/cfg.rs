use bevy::ecs::system::EntityCommands;
use jam_common::tower::{TowerTable, TowerTableItem};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Attack {
    pub radius: f32,
    pub damage: f32,
    pub cost: f64,
    pub rate: f64,
}

#[derive(Serialize, Deserialize)]
pub struct Collect {
    pub radius: f32,
    pub boost: f64,
}

#[derive(Serialize, Deserialize)]
pub struct Bank {
    pub charge: f64,
}

#[derive(Serialize, Deserialize)]
pub struct Tower {
    pub upgrades: Option<String>,
    pub name: Option<String>,
    pub desc: Option<String>,
    pub cost: f64,
    pub rate: f64,
    pub attack: Option<Attack>,
    pub collect: Option<Collect>,
    pub capacity: Option<f64>,
    pub bank: Option<Bank>,
}

impl Tower {
    pub fn spawn(&self, build: &mut EntityCommands, owner: &str, prev_capacity: f64, done: bool) {
        let t = self;

        build.insert(super::Assembly {
            rate: t.rate,
            cost: t.cost,
            progress: if done { 1. } else { 0. },
        });

        if let Some(attack) = &t.attack {
            build.insert(super::AttackTower {
                damage: attack.damage,
                radius: attack.radius,
            });
            build.insert(super::SubAssemblies(vec![super::Assembly {
                rate: attack.rate,
                cost: attack.cost,
                progress: 1.,
            }]));
        }

        if let Some(collect) = &t.collect {
            build.insert(super::CollectorTower {
                boost: collect.boost,
                radius: collect.radius,
            });
        }

        if let Some(bank) = &t.bank {
            build.insert(super::BankTower {
                owner: owner.to_owned(),
                charge: bank.charge,
            });
        }

        if let Some(capacity) = t.capacity {
            build.insert(super::Capacitor {
                prev_capacity,
                capacity,
            });
        } else if prev_capacity > 0. {
            build.insert(super::Capacitor {
                prev_capacity,
                capacity: 0.,
            });
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Towers(pub HashMap<String, Tower>);

impl<'a> From<&'a Towers> for TowerTable {
    fn from(Towers(towers): &Towers) -> Self {
        // let mut upgrade_options = HashMap::<&String, Vec<String>>::new();
        // for (id, tower) in towers.iter() {
        //    if let Some(parent) = &tower.upgrades {
        //        upgrade_options.entry(parent).or_default().push(id.clone());
        //    }
        // }

        TowerTable {
            list: towers
                .into_iter()
                .map(|(id, tower)| TowerTableItem {
                    name: tower.name.clone().unwrap_or_else(|| id.clone()),
                    description: tower.desc.clone().unwrap_or(String::new()),
                    id: id.to_string(),
                    can_place: tower.upgrades.is_none(),
                    upgrades_from: tower.upgrades.clone(),
                })
                .collect(),
        }
    }
}

pub fn read_towers() -> Result<Towers, anyhow::Error> {
    Ok(Towers(toml::from_slice(&std::fs::read(
        "./assets/towers.toml",
    )?)?))
}
