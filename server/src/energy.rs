use bevy::app::Events;
use bevy::prelude::*;
use jam_common::energy::*;
use jam_common::game::PlayerState;
use jam_common::net::prelude::*;
use jam_common::tower::Tower;
use std::ops::DerefMut;

pub const BASE_CAPACITY: f64 = 15.0;
pub const BASE_CURRENT: f64 = 15.0;
pub const BASE_PRESSURE: f64 = 0.5;

pub struct Deposit {
    pub charge: f64,
    pub pressure: f64,
}

pub struct Capacitor {
    pub capacity: f64,
    pub prev_capacity: f64,
}

fn add_charge(to: &mut Energy, charge: f64, pressure: f64) {
    let efficency = (pressure - to.pressure).max(0.).tanh();
    to.charge += charge * efficency;
}

fn update_capacity_and_charge(
    mut global: ResMut<Energy>,
    caps: Query<(&Capacitor, Option<&Assembly>)>,
    mut deps: EventReader<Deposit>,
) {
    global.total_capacity = BASE_CAPACITY;
    for (cap, asmb) in caps.iter() {
        let percent = match asmb {
            Some(asmb) => asmb.progress,
            None => 1.,
        };
        global.total_capacity += cap.capacity * percent + cap.prev_capacity * (1. - percent);
    }

    add_charge(&mut *global, BASE_CURRENT * crate::SIM_DT, BASE_PRESSURE);
    for &Deposit { charge, pressure } in deps.iter() {
        add_charge(&mut *global, charge, pressure);
    }

    global.pressure = (global.charge / global.total_capacity).max(0.);
}

fn update_assembly(
    mut global: ResMut<Energy>,
    mut asmbs: Query<&mut Assembly>,
    mut sub_asmbs: Query<&mut SubAssemblies>,
) {
    fn update_asmb(mut asmb: impl DerefMut<Target = Assembly>, global: &mut Energy) {
        if !asmb.is_running() {
            return;
        }

        let pressure = if asmb.rate > 0. { global.pressure } else { 1. };

        let mut done = asmb.get_charge();
        let delta = asmb.rate * pressure * crate::SIM_DT;
        done += delta;
        if delta > 0. {
            global.charge -= delta;
        } else {
            // Raising a tower takes away 50%
            global.charge -= 0.5 * delta;
        }
        asmb.set_charge(done);
    }

    for asmb in asmbs.iter_mut() {
        update_asmb(asmb, &mut *global);
    }

    for mut asmb in sub_asmbs.iter_mut() {
        for sub in &mut asmb.0 {
            update_asmb(sub, &mut *global);
        }
    }
}

fn boost_assembly(
    players: Res<UserMap<PlayerState>>,
    mut energy: ResMut<UserMap<PlayerEnergy>>,
    // TODO: use position component instead of tower
    mut asmbs: Query<(&Tower, &mut Assembly)>,
    mut sub_asmbs: Query<(&Tower, &mut SubAssemblies)>,
) {
    fn update_asmb(mut asmb: impl DerefMut<Target = Assembly>, player: &mut PlayerEnergy) {
        if !asmb.is_running() || asmb.rate <= 0. {
            return;
        }

        let mut done = asmb.get_charge();
        let delta = asmb.rate * player.pressure * crate::SIM_DT;
        let delta = player.charge.min(delta);
        done += delta;
        player.charge -= delta;
        asmb.set_charge(done);
    }

    for (id, player) in players.iter() {
        for (tow, asmb) in asmbs.iter_mut() {
            if player.boosting.contains(&tow.pos) {
                update_asmb(asmb, energy.entry(id.clone()).or_default());
            }
        }
    }

    for (id, player) in players.iter() {
        for (tow, mut asmb) in sub_asmbs.iter_mut() {
            if player.boosting.contains(&tow.pos) {
                for sub in &mut asmb.0 {
                    update_asmb(sub, energy.entry(id.clone()).or_default());
                }
            }
        }
    }
}

pub struct EnergyPlugin;

impl Plugin for EnergyPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.init_resource::<Events<Deposit>>();
        app.init_resource::<UserMap<PlayerEnergy>>();
        app.insert_resource(Energy {
            charge: BASE_CAPACITY * BASE_PRESSURE,
            ..Default::default()
        });
        app.enable_send(ResHook::<Energy>::UNRELIABLE);
        app.enable_send(CompHook::<Assembly>::UNRELIABLE);
        app.enable_send(MappedResHook::<PlayerEnergy>::RETAIN);
        app.add_system_to_stage(
            crate::SIM,
            update_capacity_and_charge.system().label("energy0"),
        );
        app.add_system_to_stage(
            crate::SIM,
            update_assembly.system().label("energy1").after("energy0"),
        );
        app.add_system_to_stage(crate::SIM, boost_assembly.system().after("energy0"));
    }
}
