use cgmath::Vector2;
use jam_common::map::{Grid, GridTile};
use rand::{prelude::*, thread_rng};
use std::vec::Vec;

fn get_new_pos(
    width: i32,
    height: i32,
    pos: Vector2<i32>,
    grid: &Grid<usize>,
) -> Option<(Vector2<i32>, usize)> {
    let mut rng = thread_rng();

    // get adjacent side pos
    let mut sides = Vec::new();
    if pos.y < height - 1 {
        sides.push((pos + Vector2 { x: 0, y: 1 }, 0, 2));
        //sides.push(((pos.x, pos.y + 1.0), 0, 2))
    };
    if pos.x < width - 1 {
        sides.push((pos + Vector2 { x: 1, y: 0 }, 0, 3));
        //sides.push(((pos.x + 1, px: os.y: y), 0, 3))
    };
    if pos.y > 0 {
        sides.push((pos + Vector2 { x: 0, y: -1 }, 0, 4));
        //sides.push(((pos.x, pos.yx:  - y: 1), 0, 4))
    };
    if pos.x > 0 {
        sides.push((pos + Vector2 { x: -1, y: 0 }, 0, 5));
        //sides.push(((pos.x - 1.0, pos.y), 0, 5))
    };

    // get adjacent side vals
    for mut side in &mut sides {
        side.1 = grid.get(side.0);
    }

    // ignore sides already traversed
    let mut valid_sides = Vec::new();
    for side in sides {
        if side.1 == 0 {
            valid_sides.push((side.0, side.2))
        }
    }

    if valid_sides.len() != 0 {
        // get random number
        let rand_num = rng.gen_range(0..valid_sides.len());

        let new_side = valid_sides[rand_num];

        Some(new_side)
    } else {
        None
    }
}

fn generate_roads(width: i32, height: i32, map_grid: &mut Grid<GridTile>, map: &mut Map) {
    let mut stack = Vec::new();

    map.pathfinding_grid.set(map.home, 1);

    let mut pos = map.home;

    // UNSEEN: 0, GOAL: 1, N: 2, W: 3, S: 4, E: 5

    // randomized dfs
    loop {
        // get adjacent side pos
        if let Some(new_pos) = get_new_pos(width, height, pos, &map.pathfinding_grid) {
            // println!("MOVING: Current position: {:?}", pos);
            stack.push(pos);
            map.pathfinding_grid.set(new_pos.0, new_pos.1);
            pos = new_pos.0;
        } else {
            // println!("BACKTRACKING: Current position: {:?}", pos);
            if let Some(old_pos) = stack.pop() {
                pos = old_pos;
            } else {
                break;
            }
        }
    }

    // UNSEEN: 0, GOAL: 1, N: 2, W: 3, S: 4, E: 5

    // create roads
    for enemy_spawn in &map.enemy_spawns {
        // println!("Creating road");
        let mut pos = *enemy_spawn;
        loop {
            if let GridTile::Road = map_grid.get(pos) {
                break;
            }
            if pos == map.home {
                break;
            }
            map_grid.set(pos, GridTile::Road);
            let direction = map.pathfinding_grid.get(pos);
            pos = match direction {
                2 => pos + Vector2 { x: 0, y: -1 },
                //2 => (pos.0, pos.1 - 1),
                3 => pos + Vector2 { x: -1, y: 0 },
                //3 => (pos.0 - 1, pos.1),
                4 => pos + Vector2 { x: 0, y: 1 },
                //4 => (pos.0, pos.1 + 1),
                5 => pos + Vector2 { x: 1, y: 0 },
                //5 => (pos.0 + 1, pos.1),
                _ => unreachable!(),
            };
        }
    }
}

#[derive(Debug)]
pub struct Map {
    home: Vector2<i32>,
    pub enemy_spawns: Vec<Vector2<i32>>,
    pub pathfinding_grid: Grid<usize>,
}

impl Map {
    pub fn new(width: i32, height: i32, enemy_spawns: i32) -> (Map, Grid<GridTile>) {
        let mut rng = thread_rng();

        if (height % 2 == 0) || (width % 2 == 0) {
            panic!("Map height and width need to be odd");
        }

        // verify enemy spawns can fit
        let perimiter = (width * 2) + (height * 2) - 2;
        if enemy_spawns > perimiter {
            panic!("Enemy spawns must be <= map perimiter");
        }

        // create map
        let mut grid = Grid::new(width, height);
        grid.iter_mut().for_each(|t| *t.value = GridTile::Build);
        let mut map = Map {
            home: Vector2 {
                x: width / 2,
                y: height / 4,
            },
            enemy_spawns: Vec::with_capacity(enemy_spawns as usize),
            pathfinding_grid: Grid::<usize>::new_init(width, height, 0),
        };

        // set home
        // println!("HOME: {:?}", map.home);
        grid.set(map.home, GridTile::Home);

        // println!("Generating enemy spawns");
        // create enemy spawns
        for _ in 0..enemy_spawns {
            loop {
                let spawn;
                match rng.gen_range(0..4) {
                    s @ (0 | 1) => {
                        // left or right
                        let y_pos = rng.gen_range(0..height);
                        spawn = Vector2 {
                            x: s * (width - 1),
                            y: y_pos,
                        };
                    }
                    mut s @ (2 | 3) => {
                        // top or bottom
                        s -= 2;
                        let x_pos = rng.gen_range(0..width);
                        spawn = Vector2 {
                            x: x_pos,
                            y: s * (height - 1),
                        };
                    }
                    _ => unreachable!(),
                }
                // println!("Candidate spawn: {:?}", spawn);
                // if square is already an enemy spawn, try again
                if let GridTile::EnemySpawn = grid.get(spawn) {
                    continue;
                } else {
                    // println!("Generated spawn at {:?}", spawn);
                    map.enemy_spawns.push(spawn);
                    break;
                }
            }
        }

        // println!("Generating roads");
        generate_roads(width, height, &mut grid, &mut map);

        for spawn in &map.enemy_spawns {
            grid.set(*spawn, GridTile::EnemySpawn);
        }

        (map, grid)
    }
}
