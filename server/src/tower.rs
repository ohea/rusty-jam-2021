use crate::energy::Capacitor;
use bevy::prelude::*;
use cgmath::InnerSpace;
use jam_common::enemy::Enemy;
use jam_common::energy::{Assembly, PlayerEnergy, SubAssemblies};
use jam_common::game::Game;
use jam_common::map::{Grid, GridTile};
use jam_common::net::prelude::*;
use jam_common::tower::{AttackEvent, NewTower, ToggleTower, Tower, TowerTable};

pub mod cfg;

pub struct AttackTower {
    pub damage: f32,
    pub radius: f32,
}

pub struct CollectorTower {
    pub boost: f64,
    pub radius: f32,
}

pub struct BankTower {
    pub owner: String,
    pub charge: f64,
}

pub fn spawn_requested_towers(
    mut com: Commands,
    mut new_towers: Recvs<NewTower>,
    existing: Query<(Entity, &Tower, Option<&Assembly>, Option<&Capacitor>)>,
    grid: Res<Grid<GridTile>>,
    cfg: Res<cfg::Towers>,
    game: Res<Game>,
) {
    if !game.is_playable() {
        return;
    }

    for tower in new_towers.iter() {
        match grid.get(tower.pos) {
            GridTile::Build => (),
            _ => continue,
        }

        let cfg = match cfg.0.get(&tower.id) {
            Some(c) => c,
            None => continue,
        };

        let mut prev_capacity = 0.;
        if let Some((ent, existing, asemb, cap)) = existing.iter().find(|t| t.1.pos == tower.pos) {
            if cfg.upgrades.as_ref() == Some(&existing.id) {
                if !asemb.map(|a| a.is_done()).unwrap_or(true) {
                    // can't upgrade an incomplete tower
                    continue;
                }

                com.entity(ent).despawn();
                if let Some(cap) = cap {
                    prev_capacity = cap.capacity;
                }
            } else {
                // is not an upgrade to the existing tower
                continue;
            }
        } else if cfg.upgrades.is_some() {
            // can not build a tower upgrade from scratch
            continue;
        }

        cfg.spawn(
            com.spawn().insert(Tower {
                pos: tower.pos,
                id: tower.id.clone(),
            }),
            &tower.from_id,
            prev_capacity,
            false,
        );
    }
}

fn do_respawn_parent_tower(com: &mut Commands, cfg: &cfg::Towers, tower: &Tower) -> Option<()> {
    let current = cfg.0.get(&tower.id)?;
    let parent_id = current.upgrades.as_ref()?;
    let parent = cfg.0.get(parent_id)?;
    let grandparent = parent.upgrades.as_ref().and_then(|id| cfg.0.get(id));
    let prev_capacity = grandparent.and_then(|g| g.capacity).unwrap_or(0.);

    parent.spawn(
        com.spawn().insert(Tower {
            pos: tower.pos,
            id: parent_id.clone(),
        }),
        "", // TODO: actually figure out the owner
        prev_capacity,
        true,
    );

    // TODO: continue unbuilding?

    Some(())
}

pub fn despawn_requested_towers(
    mut com: Commands,
    mut togs: Recvs<ToggleTower>,
    mut towers: Query<(Entity, &Tower, &mut Assembly)>,
    cfg: Res<cfg::Towers>,
    game: Res<Game>,
) {
    if !game.is_playable() {
        return;
    }

    let togs = togs.iter().map(|t| t.pos).collect::<Vec<_>>();
    for (ent, tower, mut asmb) in towers.iter_mut() {
        if togs.contains(&tower.pos) {
            asmb.rate *= -1.;
        }

        if asmb.is_done() && asmb.rate < 0. {
            com.entity(ent).despawn();
            do_respawn_parent_tower(&mut com, &*cfg, tower);
        }
    }
}

pub fn attack_enemies_with_tower(
    mut enemies: Query<(Entity, &mut Enemy)>,
    mut towers: Query<(
        Entity,
        &Tower,
        &AttackTower,
        Option<&BankTower>,
        &Assembly,
        &mut SubAssemblies,
    )>,
    mut attacks: Sends<AttackEvent>,
    mut players: ResMut<UserMap<PlayerEnergy>>,
) {
    for (t_id, tower, attack, bank, asmb, mut subs) in towers.iter_mut() {
        if !asmb.is_done() || !subs.is_done(0) {
            continue;
        }

        for (e_id, mut enemy) in enemies.iter_mut() {
            if enemy.health <= 0. {
                continue;
            }

            let diff = enemy.pos - tower.pos;
            if diff.cast::<f32>().unwrap().magnitude() <= attack.radius {
                enemy.health -= attack.damage;

                subs.drain(0);

                if let Some(BankTower { charge, owner }) = bank {
                    let player = players.entry(owner.clone()).or_default();
                    player.charge += *charge;
                }

                attacks.send(
                    AttackEvent {
                        source: t_id.to_bits(),
                        target: e_id.to_bits(),
                        duration: 0.2,
                        target_pos: enemy.pos,
                        color: if bank.is_some() {
                            [128, 128, 255]
                        } else {
                            [255, 255, 255]
                        },
                    }
                    .into(),
                );
                break;
            }
        }
    }
}

pub struct TowerPlugin;

impl Plugin for TowerPlugin {
    fn build(&self, app: &mut AppBuilder) {
        match cfg::read_towers() {
            Ok(towers) => {
                app.insert_resource(TowerTable::from(&towers));
                app.insert_resource(towers);
            }
            Err(e) => eprintln!("Could not load assets/towers.toml: {}", e),
        }

        app.add_system(spawn_requested_towers.system());
        app.add_system(despawn_requested_towers.system());
        app.add_system_to_stage(crate::SIM, attack_enemies_with_tower.system());
        app.enable_send(CompHook::<Tower>::RELIABLE);
        app.enable_send(ResHook::<TowerTable>::RELIABLE);
        app.enable_send(EventHook::<AttackEvent>::ORDERED);
        app.enable_recv(EventHook::<NewTower>::ORDERED);
        app.enable_recv(EventHook::<ToggleTower>::ORDERED);
    }
}
