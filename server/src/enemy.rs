use crate::energy::Deposit;
use crate::map::Map;
use crate::tower::CollectorTower;
use bevy::prelude::*;
use cgmath::InnerSpace;
use cgmath::Vector2;
use jam_common::enemy::{Enemy, PlayerHealth};
use jam_common::map::{Grid, GridTile};
use jam_common::prelude::*;
use jam_common::tower::Tower;

pub mod cfg;

struct MoveTimer(Timer);
pub struct EnemyTimer(Timer);

fn kill_enemies(
    mut com: Commands,
    mut deps: EventWriter<Deposit>,
    enemies: Query<(Entity, &Enemy)>,
    collectors: Query<(&Tower, &CollectorTower)>,
) {
    for (entity, enemy) in enemies.iter() {
        if enemy.health <= 0. {
            let mut pressure = 1.2;
            for (tower, boost) in collectors.iter() {
                if (tower.pos - enemy.pos).float().magnitude() <= boost.radius {
                    pressure += boost.boost;
                }
            }

            deps.send(Deposit {
                charge: enemy.worth,
                pressure,
            });
            com.entity(entity).despawn();
        }
    }
}

fn get_next_pos(pos: Vector2<i32>, map: &Map) -> Vector2<i32> {
    pos + match map.pathfinding_grid.try_get(pos) {
        Some(1) => Vector2 { x: 0, y: 0 },
        Some(2) => Vector2 { x: 0, y: -1 },
        Some(3) => Vector2 { x: -1, y: 0 },
        Some(4) => Vector2 { x: 0, y: 1 },
        Some(5) => Vector2 { x: 1, y: 0 },
        Some(_) => unreachable!(),
        None => unreachable!(),
    }
}

fn move_enemies(
    mut com: Commands,
    mut enemies: Query<(&mut Enemy, &mut EnemyTimer, Entity)>,
    mut player_health: ResMut<PlayerHealth>,
    map: Res<Map>,
    grid: Res<Grid<GridTile>>,
) {
    for (mut enemy, mut move_timer, ent) in enemies.iter_mut() {
        if move_timer.0.tick(crate::SIM_DELTA).just_finished() {
            if grid.get(enemy.pos) == GridTile::Home {
                player_health.0 -= 5;
                com.entity(ent).despawn();
            }
            enemy.pos = enemy.next_pos;
            enemy.next_pos = get_next_pos(enemy.next_pos, &map);
        }
    }
}

pub fn spawn(com: &mut Commands, pos: Vector2<i32>, map: &Map, list: &cfg::Enemies, mob: &str) {
    let mob = match list.0.get(mob) {
        Some(m) => m,
        None => return,
    };

    let mut color = mob.color;
    for c in &mut color {
        *c /= 255.;
    }

    com.spawn()
        .insert(Enemy {
            color,
            pos,
            next_pos: get_next_pos(pos, &map),
            move_speed: mob.move_speed,
            worth: mob.worth,
            damage: mob.damage,
            health: mob.health,
            max_health: mob.health,
        })
        .insert(EnemyTimer(Timer::from_seconds(
            mob.move_speed.recip(),
            true,
        )));
}

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut AppBuilder) {
        match cfg::read_mobs() {
            Ok(mobs) => {
                app.insert_resource(mobs);
            }
            Err(e) => eprintln!("Could not load assets/mobs.toml: {}", e),
        }

        app.enable_send(CompHook::<Enemy>::UNRELIABLE);
        app.insert_resource(MoveTimer(Timer::from_seconds(1.0, true)));
        app.insert_resource(PlayerHealth(100));
        app.enable_send(ResHook::<PlayerHealth>::UNRELIABLE);
        app.add_system_to_stage(crate::SIM, kill_enemies.system());
        app.add_system_to_stage(crate::SIM, move_enemies.system());
    }
}
