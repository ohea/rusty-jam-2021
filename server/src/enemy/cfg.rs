use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Enemy {
    pub color: [f32; 3],
    pub move_speed: f32,
    pub health: f32,
    pub damage: f32,
    pub worth: f64,
}

#[derive(Serialize, Deserialize)]
pub struct Enemies(pub HashMap<String, Enemy>);

pub fn read_mobs() -> Result<Enemies, anyhow::Error> {
    Ok(Enemies(toml::from_slice(&std::fs::read(
        "./assets/mobs.toml",
    )?)?))
}
