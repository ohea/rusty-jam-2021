use bevy::prelude::*;
use jam_common::game::{Game, GameState, PlayerState};
use jam_common::net::prelude::*;

use crate::wave::Waves;

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.init_resource::<Game>();
        app.enable_recv(MappedResHook::<PlayerState>::CLEANUP);
        app.enable_send(ResHook::<Game>::RELIABLE);
        app.add_system_to_stage(
            crate::SIM,
            change_ready_state.system().label("change_ready"),
        );
        app.add_system_to_stage(crate::SIM, change_game_state.system().after("change_ready"));
    }
}

fn change_ready_state(players: Res<UserMap<PlayerState>>, mut game: ResMut<Game>) {
    if game.is_changed() || players.is_changed() {
        game.ready_state.clear();
        let wave_num = game.wave_num;
        for (id, player) in players.iter() {
            game.ready_state
                .insert(id.clone(), player.ready_for > wave_num);
        }
    }
}

fn change_game_state(mut game: ResMut<Game>, mut wave: ResMut<Waves>) {
    match game.state {
        GameState::Playing => {
            if game.countdown.is_some() {
                game.countdown = None;
            }

            // TODO: this does not wait for all enemies to die
            if wave.is_completed() {
                game.state = GameState::Waiting;
                game.wave_num += 1;
                for state in game.ready_state.values_mut() {
                    *state = false;
                }
            }
        }
        GameState::Before => {
            if game.ready_state.len() > 0 && game.ready_state.values().all(|ready| *ready) {
                game.wave_num += 1;
                game.state = GameState::Waiting;
            }
        }
        GameState::Waiting => {
            if game.ready_state.len() == 0 {
                // no players connected, do not start
                return;
            }

            let mut ready = false;

            let remaining =
                wave.time_between.duration().as_secs_f32() * wave.time_between.percent_left();
            let remaining = remaining.round() as i32;
            if Some(remaining) != game.countdown {
                game.countdown = Some(remaining);
            }
            ready |= wave.time_between.tick(crate::SIM_DELTA).just_finished();

            ready |= game.ready_state.values().all(|ready| *ready);

            if ready {
                game.state = GameState::Playing;
                wave.increment();
            }
        }
    }
}
