use bevy::core::{Stopwatch, Timer};
use serde::Deserialize;

#[derive(Deserialize, Clone)]
pub struct Group {
    pub mob: String,
    pub count: usize,
    pub duration: f32,
    #[serde(default)]
    pub delay: f32,
}

#[derive(Deserialize, Clone)]
pub struct Wave {
    pub groups: Vec<Group>,
}

#[derive(Deserialize)]
struct WaveFilenames {
    names: Vec<String>,
    time_between: f32,
}

pub fn read_waves() -> Result<super::Waves, anyhow::Error> {
    use std::fs;
    let wave_names: WaveFilenames = toml::from_slice(&fs::read("assets/waves/waves.toml")?)?;

    let waves = wave_names
        .names
        .iter()
        .map(|wave_name| {
            Ok(toml::from_slice(&fs::read(&format!(
                "assets/waves/{}.toml",
                wave_name
            ))?)?)
        })
        .collect::<Result<_, anyhow::Error>>()?;

    Ok(super::Waves {
        current_wave: 0,
        current_time: Stopwatch::new(),
        spawned_count: Vec::new(),
        game_started: false,
        time_between: Timer::from_seconds(wave_names.time_between, true),
        waves,
    })
}
