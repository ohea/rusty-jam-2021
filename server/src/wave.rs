use crate::enemy::{cfg::Enemies, spawn};
use crate::map::Map;
use bevy::{core::Stopwatch, prelude::*};
use jam_common::game::{Game, GameState};
use rand::{seq::IteratorRandom, thread_rng};
use std::ops::{Div as _, Mul as _, Sub as _};

pub struct WavePlugin;

pub mod cfg;

pub struct Waves {
    waves: Vec<cfg::Wave>,
    spawned_count: Vec<usize>,
    current_wave: usize,
    current_time: Stopwatch,
    game_started: bool,
    pub(crate) time_between: Timer,
}

impl Waves {
    pub fn current(&self) -> Option<&cfg::Wave> {
        self.waves.get(self.current_wave)
    }

    pub fn increment(&mut self) {
        if self.game_started {
            self.current_time.reset();
            self.current_wave += 1;
        } else {
            self.game_started = true;
        }
        self.spawned_count = vec![0; self.current().map(|w| w.groups.len()).unwrap_or(0)];
    }

    pub fn is_completed(&self) -> bool {
        let wave = match self.current() {
            Some(w) => w,
            None => return false,
        };

        wave.groups
            .iter()
            .zip(&self.spawned_count)
            .all(|(group, &done)| done >= group.count)
    }
}

impl Plugin for WavePlugin {
    fn build(&self, app: &mut AppBuilder) {
        match cfg::read_waves() {
            Ok(waves) => {
                app.insert_resource(waves);
            }
            Err(e) => eprintln!("Could not load assets/waves.toml: {}", e),
        }

        app.add_system_to_stage(crate::SIM, spawn_enemies.system());
    }
}

fn spawn_enemies(
    mut com: Commands,
    map: Res<Map>,
    enemies: Res<Enemies>,
    mut waves: ResMut<Waves>,
    game: Res<Game>,
) {
    if let GameState::Waiting = game.state {
        return;
    }

    let mut rng = thread_rng();
    let waves = &mut *waves;

    let wave = match waves.waves.get(waves.current_wave) {
        Some(w) => w,
        None => return,
    };

    let t = waves.current_time.tick(crate::SIM_DELTA).elapsed_secs();
    for (group, spawned) in wave.groups.iter().zip(&mut waves.spawned_count) {
        let target = t
            .sub(group.delay)
            .max(0.)
            .div(group.duration)
            .min(1.)
            .mul(group.count as f32) as usize;
        for _ in (*spawned)..target {
            if let Some(&pos) = map.enemy_spawns.iter().choose(&mut rng) {
                spawn(&mut com, pos, &*map, &*enemies, &group.mob);
            }
        }

        *spawned = target;
    }
}
