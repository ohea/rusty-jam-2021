use cgmath::Vector2;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct NewTower {
    pub id: String,
    pub pos: Vector2<i32>,
}

#[derive(Serialize, Deserialize)]
pub struct ToggleTower {
    pub pos: Vector2<i32>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Tower {
    pub id: String,
    pub pos: Vector2<i32>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct AttackEvent {
    pub source: u64,
    pub target: u64,
    pub duration: f32,
    pub target_pos: Vector2<i32>,
    pub color: [u8; 3],
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct TowerTableItem {
    pub id: String,
    pub name: String,
    pub description: String,
    pub can_place: bool,
    pub upgrades_from: Option<String>,
}

#[derive(Default, Deserialize, Serialize, Debug, Clone)]
pub struct TowerTable {
    pub list: Vec<TowerTableItem>,
}
