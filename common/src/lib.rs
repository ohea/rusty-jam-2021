use cgmath::Vector2;
pub use log;
pub use pretty_env_logger;

pub mod enemy;
pub mod energy;
pub mod entity_mapping;
pub mod game;
pub mod map;
pub mod net;
mod oor;
pub mod tower;

pub mod prelude {
    pub use crate::net::prelude::*;
    pub use crate::VectorExt as _;
}

pub const DEFAULT_PORT: u16 = 12351;

pub trait VectorExt {
    type Float;
    type Bevy;
    type CgMath;

    fn bevy(self) -> Self::Bevy;
    fn cgmath(self) -> Self::CgMath;
    fn float(self) -> Self::Float;
}

impl VectorExt for Vector2<f32> {
    type Float = Self;
    type Bevy = bevy_math::Vec2;
    type CgMath = Self;

    fn float(self) -> Self::Float {
        self
    }

    fn bevy(self) -> Self::Bevy {
        bevy_math::Vec2::new(self.x, self.y)
    }

    fn cgmath(self) -> Self::CgMath {
        self
    }
}

impl VectorExt for bevy_math::Vec2 {
    type Float = Self;
    type Bevy = Self;
    type CgMath = Vector2<f32>;

    fn float(self) -> Self::Float {
        self
    }

    fn bevy(self) -> Self::Bevy {
        self
    }

    fn cgmath(self) -> Self::CgMath {
        Vector2::new(self.x, self.y)
    }
}

impl VectorExt for Vector2<i32> {
    type Float = Vector2<f32>;
    type Bevy = bevy_math::IVec2;
    type CgMath = Self;

    fn float(self) -> Self::Float {
        self.cast::<f32>().unwrap()
    }

    fn bevy(self) -> Self::Bevy {
        bevy_math::IVec2::new(self.x, self.y)
    }

    fn cgmath(self) -> Self::CgMath {
        self
    }
}

impl VectorExt for bevy_math::IVec2 {
    type Float = bevy_math::Vec2;
    type Bevy = Self;
    type CgMath = Vector2<i32>;

    fn float(self) -> Self::Float {
        self.as_f32()
    }

    fn bevy(self) -> Self::Bevy {
        self
    }

    fn cgmath(self) -> Self::CgMath {
        Vector2::new(self.x, self.y)
    }
}
