use bevy_app::{AppBuilder, CoreStage, Events, ManualEventReader, Plugin};
use bevy_ecs::component::Component;
use bevy_ecs::prelude::IntoExclusiveSystem;
use bevy_ecs::prelude::{Changed, Entity, World};
use bitflags::bitflags;
use byteorder::{ReadBytesExt, WriteBytesExt, LE};
use crossbeam_channel::{Receiver, Sender};
use laminar::{Packet, SocketEvent};
use log::{debug, error, info, trace};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::any::type_name;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::ops::{Deref, DerefMut};
use std::time::{Duration, Instant};

use crate::entity_mapping::EntityMapping;
use crate::oor::Oor;

pub mod prelude {
    pub use super::{
        AppBuilderExt as _, CompHook, EventHook, MappedResHook, Message, MyUserId, NetPlugin,
        ResHook, UserId, UserMap,
    };
    pub type Sends<'a, M> = bevy_app::EventWriter<'a, super::NSend<M>>;
    pub type Recvs<'a, M> = bevy_app::EventReader<'a, super::NRecv<M>>;
}

bitflags! {
    pub struct Dir: u8 {
        const RECV = 0b01;
        const SEND = 0b10;
        const BOTH = Self::RECV.bits | Self::SEND.bits;
    }
}

/// Wrapper to indicate a message was recieved
#[derive(Debug, Clone)]
pub struct NRecv<M> {
    pub from: SocketAddr,
    pub from_id: UserId,
    msg: M,
}

impl<M> Deref for NRecv<M> {
    type Target = M;

    fn deref(&self) -> &M {
        &self.msg
    }
}

impl<M> DerefMut for NRecv<M> {
    fn deref_mut(&mut self) -> &mut M {
        &mut self.msg
    }
}

/// Wrapper to indicate a message should be sent
#[derive(Debug, Clone)]
pub struct NSend<M> {
    pub to: Option<SocketAddr>,
    msg: M,
}

impl<M> From<M> for NSend<M> {
    fn from(msg: M) -> Self {
        NSend { to: None, msg }
    }
}

impl<M> From<(M, SocketAddr)> for NSend<M> {
    fn from((msg, addr): (M, SocketAddr)) -> Self {
        NSend {
            to: Some(addr),
            msg,
        }
    }
}

impl<M> Deref for NSend<M> {
    type Target = M;

    fn deref(&self) -> &M {
        &self.msg
    }
}

impl<M> DerefMut for NSend<M> {
    fn deref_mut(&mut self) -> &mut M {
        &mut self.msg
    }
}

/// Used to specify the user id.
pub struct MyUserId(pub String);

pub type UserId = String;
pub type UserMap<T> = HashMap<UserId, T>;

#[derive(Default)]
struct UserLookup {
    ids: HashMap<SocketAddr, UserId>,
    addrs: HashMap<UserId, SocketAddr>,
}

impl UserLookup {
    pub fn id(&mut self, addr: SocketAddr) -> &UserId {
        let addrs = &mut self.addrs;
        self.ids.entry(addr).or_insert_with(|| {
            let id = addr.to_string();
            addrs.insert(id.clone(), addr);
            id
        })
    }

    pub fn addr(&self, id: &UserId) -> Option<SocketAddr> {
        self.addrs.get(id).copied()
    }

    pub fn set_id(&mut self, addr: SocketAddr, id: UserId) {
        self.ids.insert(addr, id.clone());
        self.addrs.insert(id, addr);
    }

    pub fn remove(&mut self, addr: SocketAddr) {
        if let Some(id) = self.ids.remove(&addr) {
            self.addrs.remove(&id);
        }
    }
}

/// Status information for the networking system
#[derive(Default)]
pub struct NetStatus {
    pub peers: usize,
    pub unique_per_second: f32,
    pub packets_per_second: f32,
    pub bytes_per_second: f32,
}

/// Event for when a server/client disconnects
pub struct Disconnected(pub SocketAddr);

/// Event for when a server/client connects
pub struct Connected(pub SocketAddr);

/// Event to connect to a server/client
pub struct ConnectTo(pub SocketAddr);

// A simple trait used to indicate types which can be sent
pub trait Message: Component + Serialize + DeserializeOwned {}

impl<T> Message for T where T: Component + Serialize + DeserializeOwned {}

// Connects to the bevy event system
pub struct EventHook<T> {
    ordered: bool,
    msg: PhantomData<T>,
}

impl<T: Message> EventHook<T> {
    pub const ORDERED: &'static Self = &EventHook {
        ordered: true,
        msg: PhantomData,
    };

    pub const UNORDERED: &'static Self = &EventHook {
        ordered: false,
        msg: PhantomData,
    };
}

impl<T: Message> EcsHook for EventHook<T> {
    fn prepare(&'static self, world: &mut World, dir: Dir) {
        if dir.contains(Dir::RECV) {
            world.insert_resource(Events::<NRecv<T>>::default());
        }

        if dir.contains(Dir::SEND) {
            world.insert_resource(Events::<NSend<T>>::default());
        }
    }

    fn recv(&'static self, from: SocketAddr, msg: &[u8], world: &mut World) {
        let from_id = world
            .get_resource_mut::<UserLookup>()
            .unwrap()
            .id(from)
            .clone();
        let mut events = world.get_resource_mut::<Events<NRecv<T>>>().unwrap();
        if let Ok(msg) = bincode::deserialize::<T>(&msg) {
            events.send(NRecv { from, from_id, msg })
        }
    }

    fn send(&'static self, sender: &mut SendMessages, world: &mut World) {
        for NSend { to, msg } in world
            .get_resource_mut::<Events<NSend<T>>>()
            .unwrap()
            .drain()
        {
            sender.send(&msg, to, self.ordered, true);
        }
    }

    fn init(&'static self, _: &mut SendMessages, _: SocketAddr, _: &mut World) {}
}

impl<T: Message> Hook for EventHook<T> {
    type Msg = T;
}

// Connect messages to a bevy resource
pub struct ResHook<T> {
    reliable: bool,
    msg: PhantomData<T>,
}

impl<T: Message> ResHook<T> {
    pub const RELIABLE: &'static Self = &ResHook {
        reliable: true,
        msg: PhantomData,
    };

    pub const UNRELIABLE: &'static Self = &ResHook {
        reliable: false,
        msg: PhantomData,
    };
}

impl<T: Message + Default> EcsHook for ResHook<T> {
    fn prepare(&'static self, world: &mut World, dir: Dir) {
        if dir.contains(Dir::RECV) {
            world.insert_resource(T::default());
        }
    }

    fn recv(&'static self, _addr: SocketAddr, msg: &[u8], world: &mut World) {
        if let Ok(msg) = bincode::deserialize(msg) {
            *world.get_resource_mut::<T>().unwrap() = msg;
        }
    }

    fn send(&'static self, sender: &mut SendMessages, world: &mut World) {
        if world.is_resource_changed::<T>() {
            sender.send(
                &*world.get_resource::<T>().unwrap(),
                None,
                true,
                self.reliable,
            );
        }
    }

    fn init(&'static self, sender: &mut SendMessages, addr: SocketAddr, world: &mut World) {
        sender.send(
            &*world.get_resource::<T>().unwrap(),
            Some(addr),
            true,
            self.reliable,
        );
    }
}

impl<T: Message + Default> Hook for ResHook<T> {
    type Msg = T;
}

// Connect messages to a bevy resource, where each peer is separate
pub struct MappedResHook<T> {
    retain: bool,
    msg: PhantomData<T>,
}

impl<T: Message> MappedResHook<T> {
    pub const CLEANUP: &'static Self = &MappedResHook {
        retain: false,
        msg: PhantomData,
    };

    pub const RETAIN: &'static Self = &MappedResHook {
        retain: true,
        msg: PhantomData,
    };
}

impl<T: Message + Default> EcsHook for MappedResHook<T> {
    fn prepare(&'static self, world: &mut World, dir: Dir) {
        if dir.contains(Dir::RECV) {
            world.insert_resource(UserMap::<T>::default());
        }
    }

    fn recv(&'static self, addr: SocketAddr, msg: &[u8], world: &mut World) {
        let id = world
            .get_resource_mut::<UserLookup>()
            .unwrap()
            .id(addr)
            .clone();
        if let Ok(msg) = bincode::deserialize(msg) {
            world
                .get_resource_mut::<UserMap<T>>()
                .unwrap()
                .insert(id, msg);
        }
    }

    fn send(&'static self, sender: &mut SendMessages, world: &mut World) {
        if world.is_resource_changed::<UserMap<T>>() {
            for (id, msg) in world.get_resource::<UserMap<T>>().unwrap().iter() {
                if let Some(addr) = world.get_resource::<UserLookup>().unwrap().addr(id) {
                    sender.send(msg, Some(addr), true, true);
                }
            }
        }
    }

    fn init(&'static self, sender: &mut SendMessages, addr: SocketAddr, world: &mut World) {
        let id = world
            .get_resource_mut::<UserLookup>()
            .unwrap()
            .id(addr)
            .clone();
        if let Some(msg) = world.get_resource::<UserMap<T>>().unwrap().get(&id) {
            sender.send(msg, Some(addr), true, true);
        }
    }

    fn disconnect(&'static self, addr: SocketAddr, world: &mut World) {
        if !self.retain {
            let id = world
                .get_resource_mut::<UserLookup>()
                .unwrap()
                .id(addr)
                .clone();
            world.get_resource_mut::<UserMap<T>>().unwrap().remove(&id);
        }
    }
}

impl<T: Message + Default> Hook for MappedResHook<T> {
    type Msg = T;
}

/// Connect messages to a bevy component
pub struct CompHook<T> {
    reliable: bool,
    msg: PhantomData<T>,
}

#[derive(Serialize, Deserialize)]
pub struct EntityUpdate<'a, T> {
    pub id: u64,
    pub data: Option<Oor<'a, T>>,
}

impl<T: Message> CompHook<T> {
    pub const RELIABLE: &'static Self = &CompHook {
        reliable: true,
        msg: PhantomData,
    };

    pub const UNRELIABLE: &'static Self = &CompHook {
        reliable: false,
        msg: PhantomData,
    };
}

impl<T: Message> EcsHook for CompHook<T> {
    fn prepare(&'static self, world: &mut World, dir: Dir) {
        if dir.contains(Dir::RECV) {
            world.get_resource_or_insert_with(EntityMapping::default);
        }
    }

    fn recv(&'static self, _addr: SocketAddr, msg: &[u8], world: &mut World) {
        if let Ok(EntityUpdate { id, data }) = bincode::deserialize(msg) {
            let mapping = world.get_resource::<EntityMapping>().unwrap();
            match (data, mapping.get_entity(id)) {
                (None, Some(ent)) => {
                    world.despawn(ent);
                }
                (None, None) => (),
                (Some(data), Some(ent)) => {
                    if let Some(mut e) = world.get_entity_mut(ent) {
                        e.insert::<T>(data.assert_owned());
                    }
                }
                (Some(data), None) => {
                    let ent = world.spawn().insert(data.assert_owned()).id();
                    world
                        .get_resource_mut::<EntityMapping>()
                        .unwrap()
                        .insert(id, ent);
                }
            }
        }
    }

    fn send(&'static self, sender: &mut SendMessages, world: &mut World) {
        for (entity, data, changed) in world.query::<(Entity, &T, Changed<T>)>().iter_mut(world) {
            if !changed {
                continue;
            }
            let id = entity.to_bits();
            sender.send(
                &EntityUpdate {
                    id,
                    data: Some(Oor::Ref(data)),
                },
                None,
                true,
                self.reliable,
            );
        }

        for entity in world.removed::<T>() {
            let id = entity.to_bits();
            sender.send(&EntityUpdate::<T> { id, data: None }, None, false, true);
        }
    }

    fn init(&'static self, sender: &mut SendMessages, addr: SocketAddr, world: &mut World) {
        for (entity, data) in world.query::<(Entity, &T)>().iter_mut(world) {
            let id = entity.to_bits();
            sender.send(
                &EntityUpdate {
                    id,
                    data: Some(Oor::Ref(data)),
                },
                Some(addr),
                true,
                true,
            );
        }
    }
}

impl<T: Message> Hook for CompHook<T> {
    type Msg = EntityUpdate<'static, T>;
}

/// A trait implemented by any hook to integrate with the bevy ECS.
pub trait EcsHook: Sync + Send + 'static {
    fn prepare(&'static self, world: &mut World, dir: Dir);
    fn recv(&'static self, from: SocketAddr, msg: &[u8], world: &mut World);
    fn send(&'static self, sender: &mut SendMessages, world: &mut World);
    fn init(&'static self, sender: &mut SendMessages, addr: SocketAddr, world: &mut World);
    fn disconnect(&'static self, _addr: SocketAddr, _world: &mut World) {}
}

/// A trait implemented by any hook to integrate with the networking stack.
pub trait Hook: EcsHook + 'static {
    type Msg: Component;
}

/// Info for a single ecs hook
#[derive(Copy, Clone)]
struct EcsHookItem {
    hook: &'static dyn EcsHook,
    dir: Dir,
    ty: &'static str,
}

/// Resource for all registered ecs hooks
#[derive(Clone, Default)]
struct EcsHooks {
    list: Vec<EcsHookItem>,
}

/// Packet sent so that the other side knows our type ids
#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct JoinPacket {
    msg_types: Vec<String>,
    respond: bool,
    user_id: Option<String>,
}

struct JoinPacketEvent(Packet);

/// Information about a connected client/server
struct Destination {
    addr: SocketAddr,
    msg_types: HashMap<String, u16>,
}

/// Sends out objects to the laminar socket.
pub struct SendMessages {
    buf: Vec<u8>,
    hooks: EcsHooks,
    destinations: HashMap<SocketAddr, Destination>,
    disconnects: ManualEventReader<Disconnected>,
    sender: Sender<Packet>,
    byte_counter: usize,
    packet_counter: usize,
    uniq_packet_counter: usize,
    last_counter_reset: Instant,
    user_id: Option<String>,
}

impl SendMessages {
    pub fn new(world: &World, sender: Sender<Packet>) -> Self {
        SendMessages {
            buf: Vec::with_capacity(512),
            hooks: world.get_resource::<EcsHooks>().unwrap().clone(),
            destinations: HashMap::new(),
            disconnects: world.get_resource::<Events<_>>().unwrap().get_reader(),
            sender,
            byte_counter: 0,
            packet_counter: 0,
            uniq_packet_counter: 0,
            last_counter_reset: Instant::now(),
            user_id: world
                .get_resource::<MyUserId>()
                .map(|MyUserId(id)| id.clone()),
        }
    }

    fn connect_respond(&mut self, addr: SocketAddr, request_response: bool) {
        let join = JoinPacket {
            respond: request_response,
            msg_types: self.hooks.list.iter().map(|i| i.ty.to_owned()).collect(),
            user_id: self.user_id.clone(),
        };

        self.buf.clear();
        self.buf.write_u16::<LE>(u16::MAX).unwrap();
        bincode::serialize_into(&mut self.buf, &join).unwrap();

        debug!("Joining {} with: {:#?}", addr, join);

        self.packet_counter += 1;
        self.uniq_packet_counter += 1;
        self.byte_counter += self.buf.len();
        let _ = self
            .sender
            .send(Packet::reliable_ordered(addr, self.buf.clone(), None));
    }

    pub fn connect(&mut self, world: &mut World, addr: SocketAddr, recved: Option<&[u8]>) {
        if let Some(mut payload) = recved {
            if payload.read_u16::<LE>().ok() != Some(u16::MAX) {
                error!("Join packet from {} did not start with 0xffff.", addr);
                return;
            }

            // proccess the given join packet and respond
            if let Ok(data) = bincode::deserialize::<JoinPacket>(payload) {
                info!("Connected to {}", addr);
                debug!("Joined by {} with: {:#?}", addr, data);

                if let Some(id) = data.user_id {
                    world
                        .get_resource_mut::<UserLookup>()
                        .unwrap()
                        .set_id(addr, id);
                }

                if data.respond {
                    self.connect_respond(addr, false);
                }

                self.destinations.insert(
                    addr,
                    Destination {
                        addr,
                        msg_types: data
                            .msg_types
                            .into_iter()
                            .enumerate()
                            .map(|(i, s)| (s, i as u16))
                            .collect(),
                    },
                );

                // handshake is now complete! send init messages
                for i in 0..self.hooks.list.len() {
                    let EcsHookItem { hook, dir, .. } = self.hooks.list[i];
                    if dir.contains(Dir::SEND) {
                        hook.init(self, addr, world);
                    }
                }

                // and notify event listeners
                world
                    .get_resource_mut::<Events<_>>()
                    .unwrap()
                    .send(Connected(addr));
            } else {
                // don't respond with a join packet in case of error
                error!("Bad join packet from {}.", addr);
                return;
            }
        } else {
            // no join packet, just send one ourself and ask for a response
            self.connect_respond(addr, true);
        }
    }

    pub fn send<T: Serialize>(
        &mut self,
        msg: &T,
        to: Option<SocketAddr>,
        ordered: bool,
        reliable: bool,
    ) {
        let sender = &self.sender;
        let byte_counter = &mut self.byte_counter;
        let packet_counter = &mut self.packet_counter;
        let mut do_send = |buf: &mut Vec<u8>, dest: &Destination| {
            let ty = type_name::<T>();
            let ty_id = match dest.msg_types.get(ty) {
                Some(&i) => i,
                None => {
                    error!(
                        "Unknown message {} for {}. Accepted types are: {:#?}",
                        ty, dest.addr, dest.msg_types
                    );
                    return;
                }
            };

            // write the type id
            let (mut ty_bytes, _) = buf.split_at_mut(2);
            ty_bytes.write_u16::<LE>(ty_id).unwrap();

            // send the packet
            let pak = match (reliable, ordered) {
                (true, true) => Packet::reliable_ordered(dest.addr, buf.clone(), None),
                (true, false) => Packet::reliable_unordered(dest.addr, buf.clone()),
                (false, true) => Packet::unreliable_sequenced(dest.addr, buf.clone(), None),
                (false, false) => Packet::unreliable(dest.addr, buf.clone()),
            };

            *packet_counter += 1;
            *byte_counter += buf.len();
            if let Err(_) = sender.send(pak) {
                error!("Packet queue for {} is overloaded or closed.", dest.addr);
            }
        };

        // prepare the buffer to write the packet
        self.buf.clear();
        // padding for type id
        self.buf.push(0);
        self.buf.push(0);
        // write the message
        bincode::serialize_into(&mut self.buf, msg).unwrap();

        // send the packet to destinations
        self.uniq_packet_counter += 1;
        let dests = &self.destinations;
        if let Some(to) = to.and_then(|addr| dests.get(&addr)) {
            trace!(
                "Sending {} to {}: {}",
                type_name::<T>(),
                to.addr,
                serde_json::to_string_pretty(msg).unwrap()
            );

            do_send(&mut self.buf, to);
        } else {
            trace!(
                "Sending {} to all({}): {}",
                type_name::<T>(),
                self.destinations.len(),
                serde_json::to_string_pretty(msg).unwrap()
            );

            for to in self.destinations.values() {
                do_send(&mut self.buf, to);
            }
        }
    }

    fn run(&mut self, world: &mut World) {
        use std::mem::replace;

        // respond to disconnects
        for Disconnected(addr) in self.disconnects.iter(world.get_resource().unwrap()) {
            self.destinations.remove(addr);
        }

        // respond to connection requests
        for ConnectTo(addr) in world
            .get_resource_mut::<Events<_>>()
            .unwrap()
            .drain()
            .collect::<Vec<_>>()
        {
            self.connect(world, addr, None);
        }

        // respond to join packets
        let joins = world
            .get_resource_mut::<Events<_>>()
            .unwrap()
            .drain()
            .collect::<Vec<_>>();
        for JoinPacketEvent(pack) in joins {
            self.connect(world, pack.addr(), Some(pack.payload()));
        }

        // actually run hooks
        for i in 0..self.hooks.list.len() {
            let EcsHookItem { hook, dir, .. } = self.hooks.list[i];
            if dir.contains(Dir::SEND) {
                hook.send(self, world);
            }
        }

        // Update statistics
        const ONE_SEC: Duration = Duration::from_secs(1);
        if self.last_counter_reset.elapsed() > ONE_SEC {
            self.last_counter_reset += ONE_SEC;

            let byte_count = replace(&mut self.byte_counter, 0);
            let unique_count = replace(&mut self.uniq_packet_counter, 0);
            let packet_count = replace(&mut self.packet_counter, 0);
            if let Some(mut stats) = world.get_resource_mut::<NetStatus>() {
                let stats = &mut *stats;
                stats.bytes_per_second = byte_count as f32;
                stats.packets_per_second = packet_count as f32;
                stats.unique_per_second = unique_count as f32;
            }
        }
    }
}

/// Processes objects coming in from the laminar socket.
pub struct RecvMessages {
    hooks: EcsHooks,
    connected: HashMap<SocketAddr, ()>,
    recver: Receiver<SocketEvent>,
}

impl RecvMessages {
    pub fn new(world: &World, recver: Receiver<SocketEvent>) -> Self {
        RecvMessages {
            hooks: world.get_resource::<EcsHooks>().unwrap().clone(),
            connected: HashMap::new(),
            recver,
        }
    }

    fn run(&mut self, world: &mut World) {
        use std::collections::hash_map::Entry::*;
        use SocketEvent::*;

        for pack in self.recver.try_iter() {
            let addr = match &pack {
                Packet(p) => p.addr(),
                Connect(a) => *a,
                Timeout(a) => *a,
                Disconnect(a) => *a,
            };

            match (pack, self.connected.entry(addr)) {
                (Timeout(_) | Disconnect(_), Occupied(e)) => {
                    info!("Disconnected from {}.", addr);
                    world
                        .get_resource_mut::<Events<_>>()
                        .unwrap()
                        .send(Disconnected(addr));

                    for EcsHookItem { hook, dir, .. } in &self.hooks.list {
                        if dir.contains(Dir::RECV) {
                            hook.disconnect(addr, world);
                        }
                    }

                    world.get_resource_mut::<NetStatus>().unwrap().peers -= 1;
                    world.get_resource_mut::<UserLookup>().unwrap().remove(addr);
                    e.remove();
                }
                (Packet(p), e) => {
                    if let Entry::Vacant(e) = e {
                        e.insert(());
                        world.get_resource_mut::<NetStatus>().unwrap().peers += 1;
                    }

                    let mut data = p.payload();
                    let ty_id = data.read_u16::<LE>().ok();
                    if ty_id == Some(u16::MAX) {
                        // Joining or rejoining
                        world
                            .get_resource_mut::<Events<_>>()
                            .unwrap()
                            .send(JoinPacketEvent(p));
                    } else {
                        // Normal packet
                        let item = ty_id.and_then(|i| self.hooks.list.get(i as usize));
                        if let Some(&EcsHookItem { hook, dir, .. }) = item {
                            if dir.contains(Dir::RECV) {
                                hook.recv(addr, data, world);
                            }
                        }
                    }
                }
                _ => (),
            }
        }
    }
}

pub struct NetPlugin(pub Sender<Packet>, pub Receiver<SocketEvent>);

impl NetPlugin {
    pub fn start(addr: impl std::net::ToSocketAddrs) -> laminar::Result<Self> {
        let mut socket = laminar::Socket::bind_with_config(
            addr,
            laminar::Config {
                heartbeat_interval: Some(Duration::from_secs_f32(0.5)),
                max_packets_in_flight: 16_384,
                ..Default::default()
            },
        )?;
        let sender = socket.get_packet_sender();
        let receiver = socket.get_event_receiver();
        std::thread::spawn(move || socket.start_polling());

        Ok(NetPlugin(sender, receiver))
    }
}

impl Plugin for NetPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.init_resource::<Events<Connected>>();
        app.init_resource::<Events<Disconnected>>();
        app.init_resource::<Events<ConnectTo>>();
        app.init_resource::<Events<JoinPacketEvent>>();
        app.init_resource::<NetStatus>();
        app.init_resource::<UserLookup>();

        for item in app
            .world()
            .get_resource::<EcsHooks>()
            .expect("register network hooks before plugin")
            .clone()
            .list
        {
            item.hook.prepare(app.world_mut(), item.dir);
        }

        let mut recv = RecvMessages::new(app.world(), self.1.clone());
        app.add_system_to_stage(
            CoreStage::PreUpdate,
            (move |w: &mut World| recv.run(w)).exclusive_system(),
        );

        let mut send = SendMessages::new(app.world(), self.0.clone());
        app.add_system_to_stage(
            CoreStage::PostUpdate,
            (move |w: &mut World| send.run(w)).exclusive_system(),
        );
    }
}

pub trait AppBuilderExt {
    fn enable_send<H: Hook>(self, hook: &'static H) -> Self;
    fn enable_recv<H: Hook>(self, hook: &'static H) -> Self;
    fn enable_both<H: Hook>(self, hook: &'static H) -> Self;
}

fn add_to_builder<H: Hook>(app: &mut AppBuilder, hook: &'static H, dir: Dir) {
    app.world_mut()
        .get_resource_or_insert_with(EcsHooks::default)
        .list
        .push(EcsHookItem {
            hook,
            dir,
            ty: type_name::<H::Msg>(),
        });
}

impl AppBuilderExt for &mut AppBuilder {
    fn enable_send<H: Hook>(self, hook: &'static H) -> Self {
        add_to_builder(self, hook, Dir::SEND);
        self
    }

    fn enable_recv<H: Hook>(self, hook: &'static H) -> Self {
        add_to_builder(self, hook, Dir::RECV);
        self
    }

    fn enable_both<H: Hook>(self, hook: &'static H) -> Self {
        add_to_builder(self, hook, Dir::BOTH);
        self
    }
}
