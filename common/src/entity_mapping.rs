use bevy_ecs::prelude::*;
use std::collections::hash_map::{Entry, HashMap};

#[derive(Default, Debug)]
pub struct EntityMapping {
    pub(crate) recv_mapping: HashMap<u64, Entity>,
    pub(crate) send_mapping: HashMap<Entity, u64>,
}

impl EntityMapping {
    pub fn insert(&mut self, id: u64, entity: Entity) -> bool {
        match self.recv_mapping.entry(id) {
            Entry::Occupied(_) => panic!("{} is alread inserted", id),
            Entry::Vacant(e) => e.insert(entity),
        };
        self.send_mapping.insert(entity, id);
        true
    }

    pub fn remove_id(&mut self, id: u64) {
        let entity = self.recv_mapping.remove(&id).unwrap();
        self.send_mapping.remove(&entity);
    }

    pub fn remove_entity(&mut self, entity: Entity) {
        let id = self.send_mapping.remove(&entity).unwrap();
        self.recv_mapping.remove(&id);
    }

    pub fn contains(&self, id: u64) -> bool {
        self.recv_mapping.contains_key(&id)
    }

    pub fn get_entity(&self, id: u64) -> Option<Entity> {
        self.recv_mapping.get(&id).copied()
    }

    pub fn get_id(&self, entity: Entity) -> Option<u64> {
        self.send_mapping.get(&entity).copied()
    }

    pub fn drain(&mut self) -> impl Iterator<Item = (u64, Entity)> + '_ {
        self.send_mapping.clear();
        self.recv_mapping.drain()
    }
}
