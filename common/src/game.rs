use cgmath::Vector2;
use std::collections::{HashMap, HashSet};

#[derive(serde::Serialize, serde::Deserialize)]
pub enum GameState {
    Before,
    Waiting,
    Playing,
}

impl Default for GameState {
    fn default() -> GameState {
        GameState::Before
    }
}

#[derive(Default, serde::Serialize, serde::Deserialize)]
pub struct Game {
    pub state: GameState,
    pub ready_state: HashMap<String, bool>,
    pub wave_num: u32,
    pub wave_active: bool,
    pub countdown: Option<i32>,
}

impl Game {
    pub fn is_playable(&self) -> bool {
        match self.state {
            GameState::Before => false,
            _ => true,
        }
    }
}

#[derive(Default, serde::Serialize, serde::Deserialize)]
pub struct PlayerState {
    pub ready_for: u32,
    pub boosting: HashSet<Vector2<i32>>,
}

