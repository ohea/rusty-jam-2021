use cgmath::Vector2;

#[derive(Clone, Copy, Debug, serde::Serialize, serde::Deserialize, PartialEq, Eq)]
pub enum GridTile {
    Road,
    Build,
    Home,
    EnemySpawn,
    Void,
}

impl Default for GridTile {
    fn default() -> GridTile {
        GridTile::Void
    }
}

pub struct TileRef<'g, T> {
    pub value: &'g T,
    pub pos: Vector2<i32>,
}

pub struct TileMut<'g, T> {
    pub value: &'g mut T,
    pub pos: Vector2<i32>,
}

#[derive(Default, Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Grid<T: Copy + Default> {
    pub width: i32,
    pub height: i32,
    grid: Vec<T>,
}

impl<T: Copy + Default> Grid<T> {
    pub fn try_get(&self, pos: Vector2<i32>) -> Option<&T> {
        if pos.x < 0 || pos.y < 0 || pos.x >= self.width || pos.y >= self.height {
            None
        } else {
            self.grid.get(((pos.y * self.width) + pos.x) as usize)
        }
    }

    pub fn try_get_mut(&mut self, pos: Vector2<i32>) -> Option<&mut T> {
        if pos.x < 0 || pos.y < 0 || pos.x >= self.width || pos.y >= self.height {
            None
        } else {
            self.grid.get_mut(((pos.y * self.width) + pos.x) as usize)
        }
    }

    pub fn get(&self, pos: Vector2<i32>) -> T {
        self.try_get(pos).copied().unwrap_or_else(Default::default)
    }

    pub fn set(&mut self, pos: Vector2<i32>, tile: T) {
        if let Some(t) = self.try_get_mut(pos) {
            *t = tile;
        }
    }

    pub fn new(width: i32, height: i32) -> Grid<T> {
        if width < 0 || height < 0 {
            panic!("Width and height cannot be negative");
        }
        Grid::<T> {
            width,
            height,
            //grid: Vec::<T>::with_capacity(width * height),
            grid: vec![T::default(); (width * height) as usize],
        }
    }

    pub fn new_init(width: i32, height: i32, init: T) -> Grid<T> {
        if width < 0 || height < 0 {
            panic!("Width and height cannot be negative");
        }
        Grid::<T> {
            width,
            height,
            grid: vec![init; (width * height) as usize],
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = TileRef<T>> + '_ {
        let w = self.width;
        self.grid.iter().enumerate().map(move |(i, value)| {
            let x: i32 = i as i32 % w;
            let y: i32 = i as i32 / w;
            TileRef {
                pos: Vector2::<i32>::new(x, y),
                value,
            }
        })
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = TileMut<T>> + '_ {
        let w = self.width;
        self.grid.iter_mut().enumerate().map(move |(i, value)| {
            let x: i32 = i as i32 % w;
            let y: i32 = i as i32 / w;
            TileMut {
                pos: Vector2::<i32>::new(x, y),
                value,
            }
        })
    }
}

pub const WIDTH: i32 = 21;
pub const HEIGHT: i32 = 21;
