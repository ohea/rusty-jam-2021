use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Debug, Copy, Clone)]
pub enum Oor<'a, T> {
    Owned(T),
    Ref(&'a T),
}

impl<'a, T> Oor<'a, T> {
    pub fn assert_owned(self) -> T {
        match self {
            Oor::Owned(x) => x,
            _ => panic!(),
        }
    }
}

impl<'a, T: Serialize> Serialize for Oor<'a, T> {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Oor::Owned(x) => x.serialize(ser),
            Oor::Ref(x) => x.serialize(ser),
        }
    }
}

impl<'a, 'de, T: Deserialize<'de>> Deserialize<'de> for Oor<'a, T> {
    fn deserialize<D>(de: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Oor::Owned(T::deserialize(de)?))
    }
}
