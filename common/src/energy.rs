#[derive(Default, serde::Serialize, serde::Deserialize)]
pub struct Energy {
    pub charge: f64,
    pub total_capacity: f64,
    pub pressure: f64,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct PlayerEnergy {
    pub charge: f64,
    pub pressure: f64,
}

impl Default for PlayerEnergy {
    fn default() -> Self {
        PlayerEnergy {
            charge: 0.,
            pressure: 1.,
        }
    }
}

#[derive(Default, Copy, Clone, serde::Serialize, serde::Deserialize)]
pub struct Assembly {
    pub rate: f64,
    pub cost: f64,
    pub progress: f64,
}

impl Assembly {
    pub fn is_done(self) -> bool {
        if self.rate >= 0. {
            self.progress == 1.
        } else {
            self.progress == 0.
        }
    }

    pub fn is_running(self) -> bool {
        if self.cost <= 0. {
            return false;
        }

        if self.rate > 0. {
            self.progress != 1.
        } else if self.rate < 0. {
            self.progress != 0.
        } else {
            false
        }
    }

    pub fn get_charge(&self) -> f64 {
        self.progress * self.cost
    }

    pub fn set_charge(&mut self, charge: f64) {
        if self.cost <= 0. {
            return;
        }

        let progress = charge / self.cost;
        self.progress = progress.min(1.).max(0.);
    }

    pub fn progress_f32(self) -> f32 {
        self.progress as f32
    }
}

#[derive(Default, Clone)]
pub struct SubAssemblies(pub Vec<Assembly>);

impl SubAssemblies {
    pub fn is_done(&self, index: usize) -> bool {
        self.0[index].is_done()
    }

    pub fn drain(&mut self, index: usize) {
        self.0[index].progress = 0.;
    }
}
