use cgmath::Vector2;

#[derive(Default, serde::Serialize, serde::Deserialize)]
pub struct PlayerHealth(pub i32);

#[derive(Clone, Debug, serde:: Serialize, serde::Deserialize)]
pub struct Enemy {
    pub color: [f32; 3],
    pub move_speed: f32,
    pub max_health: f32,
    pub health: f32,
    pub pos: Vector2<i32>,
    pub next_pos: Vector2<i32>,
    pub damage: f32,
    pub worth: f64,
}
