use bevy::prelude::*;

use jam_common::map::{Grid, GridTile};
use jam_common::net::prelude::*;

struct Tile;

fn update_grid(
    mut com: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    map: Res<Grid<GridTile>>,
    existing: Query<(Entity, &Tile)>,
) {
    if !map.is_changed() {
        return;
    }

    for (entity, _) in existing.iter() {
        com.entity(entity).despawn();
    }

    for tile in map.iter() {
        let material = match tile.value {
            GridTile::Road { .. } => "default_stone.png",
            GridTile::Build => "default_grass.png",
            GridTile::Home => "default_cactus_top.png",
            GridTile::EnemySpawn => "default_gravel.png",
            GridTile::Void => continue,
        };

        com.spawn_bundle(SpriteBundle {
            sprite: Sprite::new(Vec2::new(1., 1.)),
            material: materials.add(asset_server.load(material).into()),
            transform: Transform::from_xyz(tile.pos.x as f32, tile.pos.y as f32, 80.),
            ..Default::default()
        })
        .insert(Tile);
    }
}

pub struct MapPlugin;

impl Plugin for MapPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.enable_recv(ResHook::<Grid<GridTile>>::UNRELIABLE);
        app.add_system(update_grid.system());
    }
}
