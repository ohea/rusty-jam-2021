use crate::tower::TowerMaterials;
use crate::MousePosition;
use bevy::ecs::component::Component;
use bevy::prelude::*;
use bevy_egui::{egui, EguiContext};
use cgmath::Vector2;
use jam_common::enemy::PlayerHealth;
use jam_common::energy::{Energy, PlayerEnergy};
use jam_common::game::{Game, PlayerState};
use jam_common::map::{Grid, GridTile};
use jam_common::prelude::*;
use jam_common::tower;
use jam_common::tower::TowerTable;
use std::collections::HashMap;
use std::collections::HashSet;

pub struct SelectorPlugin;

impl Plugin for SelectorPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.enable_recv(ResHook::<PlayerEnergy>::RELIABLE);
        app.enable_recv(ResHook::<Energy>::UNRELIABLE);
        app.add_startup_system(setup_tower_ghost.system());
        app.add_startup_system(SelectorMat::setup.system());
        app.init_resource::<TowerSelection>();
        app.add_system(update_tower_ghost.system());
        app.add_system(update_selector_sprites.system());
        app.add_system(show_menu.system().before("block_clicks"));
        app.add_system(
            handle_click
                .system()
                .after("block_clicks")
                .label("selector_click"),
        );
        app.add_system(update_selected_towers.system().after("selector_click"));
        app.add_system(set_boost.system());
    }
}

#[derive(Default)]
pub struct TowerSelection {
    pub id_to_place: Option<String>,
    pub selected_tiles: HashSet<Vector2<i32>>,
    pub selected_tower_ids: HashMap<String, usize>,
    pub avalible_upgrades: HashSet<String>,
}

fn show_menu(
    ctx: ResMut<EguiContext>,
    towers: Res<TowerTable>,
    mats: Res<TowerMaterials>,
    energy: Res<Energy>,
    my_energy: Res<PlayerEnergy>,
    health: Res<PlayerHealth>,
    mut select: ResMut<TowerSelection>,
    mut new: Sends<tower::NewTower>,
    game: Res<Game>,
) {
    if !game.is_playable() {
        return;
    }

    egui::Window::new("Build").show(ctx.ctx(), |ui| {
        ui.label(format!("Running at {:.1}%", energy.pressure * 100.,));
        ui.label(format!("Team Charge = {:.1}", energy.charge));
        ui.label(format!("My Charge = {:.1}", my_energy.charge));
        ui.label(format!("Health = {}", health.0));

        ui.add(egui::Label::new("Towers").heading());

        for tower in &towers.list {
            if !tower.can_place {
                continue;
            }

            let mut but = egui::widgets::Button::new(&tower.name);
            let mut selected = false;
            if Some(&tower.id) == select.id_to_place.as_ref() {
                selected = true;
                but = but.stroke((2., egui::Color32::GOLD));
            }

            if ui.add(but).clicked() {
                select.id_to_place = Some(tower.id.clone());
            }

            if selected {
                if let Some(mat) = mats.map.get(&tower.id) {
                    ui.add(egui::widgets::Image::new(
                        egui::TextureId::User(mat.for_egui),
                        [48.0, 48.0],
                    ));
                }
                ui.label(&tower.description);
            }
        }

        if select.selected_tower_ids.is_empty() {
            return;
        }

        ui.add(egui::Label::new("Upgrades").heading());

        for tower in &towers.list {
            match &tower.upgrades_from {
                Some(id) if select.selected_tower_ids.get(id).is_some() => (),
                _ => continue,
            }

            let but = egui::widgets::Button::new(&tower.name);
            if ui.add(but).clicked() {
                for &pos in &select.selected_tiles {
                    new.send(
                        tower::NewTower {
                            id: tower.id.clone(),
                            pos,
                        }
                        .into(),
                    );
                }
            }
            if let Some(mat) = mats.map.get(&tower.id) {
                ui.add(egui::widgets::Image::new(
                    egui::TextureId::User(mat.for_egui),
                    [48.0, 48.0],
                ));
            }
            ui.label(&tower.description);
        }
    });
}

struct SelectorMat {
    select: Handle<ColorMaterial>,
    boost: Handle<ColorMaterial>,
}

impl SelectorMat {
    fn setup(
        mut com: Commands,
        asset_server: Res<AssetServer>,
        mut materials: ResMut<Assets<ColorMaterial>>,
    ) {
        com.insert_resource(SelectorMat {
            select: materials.add(asset_server.load("selector.png").into()),
            boost: materials.add(asset_server.load("boost.png").into()),
        });
    }
}

fn do_update_sprites<'a, T: Component + Default, I>(
    com: &mut Commands,
    mat: &Handle<ColorMaterial>,
    mut set: Vec<Vector2<i32>>,
    ents: I,
) where
    I: Iterator<Item = (Entity, Mut<'a, Transform>, &'a T)>,
{
    for (ent, mut transform, _) in ents {
        let tra = &mut transform.translation;
        if let Some(pos) = set.pop() {
            // move to a new location
            tra.x = pos.x as f32;
            tra.y = pos.y as f32;
        } else {
            // unused selector
            com.entity(ent).despawn();
        }
    }

    // remaining selectors
    for pos in set {
        com.spawn_bundle(SpriteBundle {
            sprite: Sprite::new(Vec2::new(1., 1.)),
            material: mat.clone(),
            transform: Transform::from_xyz(pos.x as f32, pos.y as f32, 210.),
            ..Default::default()
        })
        .insert(T::default());
    }
}

#[derive(Default)]
struct Selector;

fn update_selector_sprites(
    mut com: Commands,
    mut entities: Query<(Entity, &mut Transform, &Selector)>,
    selection: Res<TowerSelection>,
    mat: Res<SelectorMat>,
) {
    if !selection.is_changed() {
        return;
    }

    let set = selection.selected_tiles.iter().copied().collect::<Vec<_>>();
    do_update_sprites::<Selector, _>(&mut com, &mat.select, set, entities.iter_mut());
}

struct TowerGhost;

fn setup_tower_ghost(mut com: Commands) {
    com.spawn_bundle(SpriteBundle {
        sprite: Sprite::new(Vec2::new(0.6, 0.6)),
        visible: Visible {
            is_visible: false,
            is_transparent: true,
        },
        transform: Transform::from_xyz(0., 0., 450.),
        ..Default::default()
    })
    .insert(TowerGhost);
}

fn update_tower_ghost(
    _com: Commands,
    mut ghosts: Query<(
        &mut Transform,
        &mut Visible,
        &mut Handle<ColorMaterial>,
        &TowerGhost,
    )>,
    selection: Res<TowerSelection>,
    mouse: Res<MousePosition>,
    mats: Res<TowerMaterials>,
    map: Res<Grid<GridTile>>,
) {
    let (mut ghost_transform, mut ghost_vis, mut ghost_mat, _) = match ghosts.single_mut() {
        Ok(g) => g,
        _ => return,
    };

    if let (Some(pos), Some(id)) = (mouse.tile_position, &selection.id_to_place) {
        ghost_vis.is_visible = true;
        ghost_transform.translation.x = pos.x as f32;
        ghost_transform.translation.y = pos.y as f32;
        if let Some(mat) = mats.map.get(id) {
            if map.get(pos) == GridTile::Build {
                *ghost_mat = mat.for_ghost.clone();
            } else {
                *ghost_mat = mat.for_error.clone();
            }
        }
    } else {
        ghost_vis.is_visible = false;
    }
}

fn handle_click(
    mouse: Res<Input<MouseButton>>,
    kb: Res<Input<KeyCode>>,
    pos: Res<MousePosition>,
    map: Res<Grid<GridTile>>,
    mut select: ResMut<TowerSelection>,
    mut new: Sends<tower::NewTower>,
    mut tog: Sends<tower::ToggleTower>,
) {
    let pos = match pos.tile_position {
        Some(p) => p,
        None => return,
    };

    if mouse.just_pressed(MouseButton::Left) {
        if let Some(id) = select.id_to_place.clone() {
            new.send(tower::NewTower { id, pos }.into());
            if !kb.pressed(KeyCode::LShift) {
                select.id_to_place = None;
            }
        } else if map.get(pos) == GridTile::Build {
            if !kb.pressed(KeyCode::LShift) {
                select.selected_tiles.clear();
            }

            if !select.selected_tiles.insert(pos) {
                select.selected_tiles.remove(&pos);
            }
        }
    }

    if mouse.just_pressed(MouseButton::Right) {
        tog.send(tower::ToggleTower { pos }.into());
    }
}

fn update_selected_towers(towers: Query<&tower::Tower>, mut select: ResMut<TowerSelection>) {
    let select = &mut *select; // deref Mut<_> only once to allow borrow splitting
    let selected_ids = &mut select.selected_tower_ids;
    selected_ids.clear();
    for tower in towers.iter() {
        if select.selected_tiles.contains(&tower.pos) {
            *selected_ids.entry(tower.id.clone()).or_insert(0) += 1;
        }
    }
}

#[derive(Default)]
struct Boost;

fn set_boost(
    mut com: Commands,
    mut player: ResMut<PlayerState>,
    kb: Res<Input<KeyCode>>,
    select: Res<TowerSelection>,
    mut entities: Query<(Entity, &mut Transform, &Boost)>,
    mat: Res<SelectorMat>,
) {
    if !kb.just_pressed(KeyCode::Space) {
        return;
    }

    player.boosting = select.selected_tiles.clone();
    let set = player.boosting.iter().copied().collect::<Vec<_>>();
    do_update_sprites::<Boost, _>(&mut com, &mat.boost, set, entities.iter_mut());
}
