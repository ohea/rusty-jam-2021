use bevy::prelude::*;
use jam_common::enemy::Enemy;

use jam_common::prelude::*;

struct LerpHelper {
    start_pos: IVec2,
    end_pos: IVec2,
    arrival: Option<Timer>,
}

fn spawn_enemies(
    mut com: Commands,
    enemies: Query<(Entity, &Enemy), Added<Enemy>>,
    mut colors: ResMut<Assets<ColorMaterial>>,
) {
    for (ent, enemy) in enemies.iter() {
        let material =
            colors.add(Color::rgb(enemy.color[0], enemy.color[1], enemy.color[2]).into());
        com.entity(ent)
            .insert_bundle(SpriteBundle {
                sprite: Sprite::new(Vec2::new(0.25, 0.25)),
                material,
                transform: Transform::from_xyz(enemy.pos.x as f32, enemy.pos.y as f32, 500.),
                ..Default::default()
            })
            .insert(start_lerp(enemy));
    }
}

fn start_lerp(prop: &Enemy) -> LerpHelper {
    let dist = (prop.next_pos - prop.pos).float().bevy().length();
    LerpHelper {
        start_pos: prop.pos.bevy(),
        end_pos: prop.next_pos.bevy(),
        arrival: if dist > 0.01 {
            Some(Timer::from_seconds(dist / prop.move_speed, false))
        } else {
            None
        },
    }
}

fn move_enemies(mut enemies: Query<(&Enemy, &mut LerpHelper), Changed<Enemy>>) {
    for (prop, mut lerp) in enemies.iter_mut() {
        if lerp.start_pos != prop.pos.bevy() {
            *lerp = start_lerp(prop);
        }
    }
}

fn lerp_enemies(mut enemies: Query<(&mut Transform, &Enemy, &mut LerpHelper)>, time: Res<Time>) {
    for (mut enemy_pos, enemy, mut lerp) in enemies.iter_mut() {
        let arrival = match &mut lerp.arrival {
            Some(a) => a,
            None => continue,
        };
        let t = arrival.tick(time.delta()).percent();
        let pos = lerp
            .start_pos
            .bevy()
            .float()
            .lerp(lerp.end_pos.bevy().float(), t);

        // We could teleport directly to pos, but it makes the movement jerky.
        // Link enemy to pos with a little spring instead, so it accels/corners smoothly.
        const SPRING_LEN: f32 = 0.15;
        const MARGIN: f32 = 0.001;

        let current_pos = enemy_pos.translation.truncate();
        let mut vec = pos - current_pos;
        let len = vec.length();
        if len < MARGIN {
            continue;
        }
        vec /= len;

        if len > SPRING_LEN + MARGIN {
            vec *= len - SPRING_LEN;
        } else {
            vec *= (len / SPRING_LEN) * enemy.move_speed * time.delta_seconds();
        }

        enemy_pos.translation += vec.extend(0.);
    }
}

fn change_enemy_color(
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut home: Query<(&Enemy, &Handle<ColorMaterial>)>,
) {
    for (enemy, cm) in &mut home.iter_mut() {
        let color_mat = materials.get_mut(cm).unwrap();
        let health_ratio = enemy.health / enemy.max_health;
        let red = enemy.color[0] * health_ratio;
        let green = enemy.color[1] * health_ratio;
        let blue = enemy.color[2] * health_ratio;
        color_mat.color = Color::rgb(red, green, blue)
    }
    return;
}

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.enable_recv(CompHook::<Enemy>::UNRELIABLE);
        app.add_system(spawn_enemies.system());
        app.add_system(move_enemies.system());
        app.add_system(lerp_enemies.system());
        app.add_system(change_enemy_color.system());
    }
}
