use bevy::prelude::*;
use bevy_egui::EguiContext;
use cgmath::num_traits::FloatConst;
use jam_common::energy::Assembly;
use jam_common::entity_mapping::EntityMapping;
use jam_common::net::prelude::*;
use jam_common::tower::{AttackEvent, NewTower, ToggleTower, Tower, TowerTable};
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::time::Duration;

pub struct TowerPlugin;

impl Plugin for TowerPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(new_towers.system().after("tower_materials"));
        app.add_system(assemble_towers.system());
        app.add_system(spawn_lasers.system());
        app.add_system(update_lasers.system());
        app.add_system(track_lasers.system());
        app.add_system(load_tower_materials.system().label("tower_materials"));
        app.init_resource::<TowerMaterials>();
        app.enable_recv(EventHook::<AttackEvent>::ORDERED);
        app.enable_send(EventHook::<ToggleTower>::ORDERED);
        app.enable_send(EventHook::<NewTower>::UNORDERED);
        app.enable_recv(CompHook::<Tower>::RELIABLE);
        app.enable_recv(ResHook::<TowerTable>::RELIABLE);
    }
}

pub struct TowerMaterial {
    pub for_world: Handle<ColorMaterial>,
    pub for_ghost: Handle<ColorMaterial>,
    pub for_error: Handle<ColorMaterial>,
    pub for_egui: u64,
}

#[derive(Default)]
pub struct TowerMaterials {
    pub map: HashMap<String, TowerMaterial>,
}

fn load_tower_materials(
    mut ctx: ResMut<EguiContext>,
    asset_server: Res<AssetServer>,
    table: Res<TowerTable>,
    mut towers: Query<(&mut Handle<ColorMaterial>, &Tower)>,
    mut mats: ResMut<TowerMaterials>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    const BASE_TEX_ID: u64 = 725472;

    if !table.is_changed() {
        return;
    }

    for tower in &table.list {
        let for_egui = mats.map.len() as u64 + BASE_TEX_ID;
        if let Entry::Vacant(e) = mats.map.entry(tower.id.clone()) {
            let tex = asset_server.load(format!("towers/{}.png", &tower.id).as_str());
            let for_world = materials.add(tex.clone().into());
            let for_ghost = materials.add(ColorMaterial {
                color: Color::rgba(1., 1., 1., 0.5),
                texture: Some(tex.clone()),
            });
            let for_error = materials.add(ColorMaterial {
                color: Color::rgba(2., 0.2, 0.2, 0.5),
                texture: Some(tex.clone()),
            });
            ctx.set_egui_texture(for_egui, tex);
            e.insert(TowerMaterial {
                for_world,
                for_ghost,
                for_error,
                for_egui,
            });
        }
    }

    for (mut tower_mat, tower) in towers.iter_mut() {
        if let Some(mat) = mats.map.get(&tower.id) {
            *tower_mat = mat.for_world.clone();
        }
    }
}

fn new_towers(
    mut com: Commands,
    towers: Query<(Entity, &Tower), Added<Tower>>,
    mats: Res<TowerMaterials>,
) {
    for (ent, tower) in towers.iter() {
        let material = if let Some(mat) = mats.map.get(&tower.id) {
            mat.for_world.clone()
        } else {
            Default::default()
        };

        com.entity(ent).insert_bundle(SpriteBundle {
            sprite: Sprite::new(Vec2::new(0.6, 0.6)),
            material,
            transform: Transform::from_xyz(tower.pos.x as f32, tower.pos.y as f32, 400.),
            ..Default::default()
        });
    }
}

fn assemble_towers(mut towers: Query<(&mut Transform, &Assembly, &Tower)>) {
    for (mut trans, asmb, _) in towers.iter_mut() {
        let p = asmb.progress_f32();
        *trans = Transform {
            translation: trans.translation,
            rotation: Quat::from_rotation_z((0.75 - 0.75 * p) * f32::PI()),
            scale: Vec3::splat(p * 0.75 + 0.5),
        };
    }
}

struct Laser {
    target: Entity,
    startpoint: Vec2,
    endpoint: Vec2,
    despawn: Timer,
}

fn track_lasers(mut lasers: Query<(&Transform, &mut Laser)>, targets: Query<&Transform>) {
    for (_laser_t, mut laser) in lasers.iter_mut() {
        if let Ok(target) = targets.get(laser.target) {
            laser.endpoint = target.translation.truncate();
        }
    }
}

fn update_lasers(
    time: Res<Time>,
    mut com: Commands,
    mut lasers: Query<(Entity, &mut Transform, &mut Laser)>,
) {
    for (laser_e, mut laser_t, mut laser) in lasers.iter_mut() {
        if laser.despawn.tick(time.delta()).finished() {
            com.entity(laser_e).despawn();
        }

        let vec = laser.endpoint - laser.startpoint;
        let angle = vec.y.atan2(vec.x);
        let mid = (laser.startpoint + laser.endpoint) / 2.;
        let a = laser.despawn.percent_left();
        let a = (a.sqrt() * f32::PI()).sin();
        *laser_t = Transform {
            translation: Vec3::new(mid.x, mid.y, 900.),
            rotation: Quat::from_rotation_z(angle),
            scale: Vec3::new(vec.length(), 0.1 * a, 1.),
        };
    }
}

fn spawn_lasers(
    mut com: Commands,
    mut events: Recvs<AttackEvent>,
    ids: Res<EntityMapping>,
    tower: Query<&Transform>,
    mut colors: ResMut<Assets<ColorMaterial>>,
) {
    for event in events.iter() {
        let (source, target) = match (ids.get_entity(event.source), ids.get_entity(event.target)) {
            (Some(s), Some(t)) => (s, t),
            _ => continue,
        };

        let source_pos = match tower.get(source) {
            Ok(s) => s,
            _ => continue,
        };

        let target_pos = match tower.get(target) {
            Ok(t) => t.translation.truncate(),
            _ => Vec2::new(event.target_pos.x as f32, event.target_pos.y as f32),
        };

        let [r, g, b] = event.color.map(|x| x as f32 / 255.);
        let material = colors.add(Color::rgb(r, g, b).into());
        com.spawn_bundle(SpriteBundle {
            sprite: Sprite::new(Vec2::splat(1.)),
            material,
            transform: Transform {
                scale: Vec3::splat(0.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Laser {
            target,
            startpoint: source_pos.translation.truncate(),
            endpoint: target_pos,
            despawn: Timer::new(Duration::from_secs_f32(event.duration), false),
        });
    }
}
