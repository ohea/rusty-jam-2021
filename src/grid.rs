use crate::Pallet;
use bevy::prelude::*;

pub struct GridPlugin;

impl Plugin for GridPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_startup_system(setup.system());
    }
}

fn setup(mut com: Commands, pallet: Res<Pallet>) {
    // draw grid
    let height = jam_common::map::HEIGHT;
    let width = jam_common::map::WIDTH;

    for i in 0..height + 1 {
        let transform = Transform {
            translation: Vec3::new(width as f32 / 2. - 0.5, i as f32 - 0.5, 90.),
            ..Default::default()
        };
        com.spawn_bundle(SpriteBundle {
            sprite: Sprite::new(Vec2::new(width as f32, 0.1)),
            material: pallet.square_edge.clone(),
            transform,
            ..Default::default()
        });
    }

    for i in 0..width + 1 {
        let transform = Transform {
            translation: Vec3::new(i as f32 - 0.5, height as f32 / 2. - 0.5, 90.),
            ..Default::default()
        };
        com.spawn_bundle(SpriteBundle {
            sprite: Sprite::new(Vec2::new(0.1 as f32, height as f32)),
            material: pallet.square_edge.clone(),
            transform,
            ..Default::default()
        });
    }
}
