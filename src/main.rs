#![allow(mixed_script_confusables)]

use bevy::input::mouse;
use bevy::math::Vec3;
use bevy::prelude::*;
use bevy::render::camera;
use bevy::transform::components::Transform;
use bevy::window;
use bevy_egui::{EguiContext, EguiPlugin};
use cgmath::Vector2;
use jam_common::enemy::PlayerHealth;
use jam_common::energy::Assembly;
use jam_common::entity_mapping::EntityMapping;
use jam_common::net;
use jam_common::net::prelude::*;
use std::net::{IpAddr, SocketAddr};

mod enemy;
mod game;
mod grid;
mod map;
mod selector;
mod tower;

/// Play a tower defense game with friends!
#[derive(argh::FromArgs)]
struct JamClient {
    /// your player name
    #[argh(positional)]
    pub name: Option<String>,

    /// the server ip address to connect to (efaults to localhost)
    #[argh(option, default = "[127, 0, 0, 1].into()", short = 's')]
    pub server: IpAddr,

    /// the server port to connect to (defaults to 12351)
    #[argh(option, default = "12351", short = 'p')]
    pub port: u16,
}

#[derive(Default)]
struct MousePosition {
    pub window_id: bevy::window::WindowId,
    pub position: Option<Vec2>,
    pub world_position: Option<Vec3>,
    pub tile_position: Option<Vector2<i32>>,
    pub window_size: Option<Vec2>,
}

pub struct Pallet {
    pub square_edge: Handle<ColorMaterial>,
}

impl FromWorld for Pallet {
    fn from_world(world: &mut World) -> Self {
        let mut colors: Mut<Assets<ColorMaterial>> = world.get_resource_mut().unwrap();
        Pallet {
            square_edge: colors.add(Color::rgb(0.1, 0.1, 0.1).into()),
        }
    }
}

fn setup(mut com: Commands, cfg: Res<JamClient>, mut connect: EventWriter<net::ConnectTo>) {
    com.spawn_bundle(OrthographicCameraBundle {
        orthographic_projection: camera::OrthographicProjection {
            scale: 20.,
            scaling_mode: camera::ScalingMode::FixedVertical,
            window_origin: camera::WindowOrigin::Center,
            depth_calculation: camera::DepthCalculation::ZDifference,
            ..Default::default()
        },
        ..OrthographicCameraBundle::new_2d()
    });

    connect.send(net::ConnectTo(SocketAddr::new(cfg.server, cfg.port)));
}

fn reload_server(
    mut com: Commands,
    keys: Res<Input<KeyCode>>,
    cfg: Res<JamClient>,
    mut connect: EventWriter<net::ConnectTo>,
    mut mapping: ResMut<EntityMapping>,
) {
    if keys.just_pressed(KeyCode::R) {
        for (_, ent) in mapping.drain() {
            com.entity(ent).despawn();
        }
        connect.send(net::ConnectTo(SocketAddr::new(cfg.server, cfg.port)));
    }
}

fn zoom_camera(
    mut scrolls: EventReader<mouse::MouseWheel>,
    mut cam: Query<(
        &mut camera::OrthographicProjection,
        &mut camera::Camera,
        &mut Transform,
    )>,
    mouse: Res<MousePosition>,
) {
    let mut total = 0.;
    for scroll in scrolls.iter() {
        total += scroll.y
            / match scroll.unit {
                mouse::MouseScrollUnit::Line => 1.,
                mouse::MouseScrollUnit::Pixel => 12.,
            };
    }

    if total == 0. {
        return;
    }
    let scale = 0.75f32.powf(total);

    let (mut proj, mut cam, mut t) = cam.single_mut().unwrap();
    proj.scale *= scale;
    if let Some(pos) = mouse.world_position {
        t.translation = scale * (t.translation - pos) + pos;
    }

    // TODO: fixed by https://github.com/bevyengine/bevy/pull/2015
    cam.projection_matrix = camera::CameraProjection::get_projection_matrix(&*proj);
}

fn slide_camera(
    mouse: Res<MousePosition>,
    time: Res<Time>,
    mut cam: Query<(
        &mut Transform,
        &camera::OrthographicProjection,
        &camera::Camera,
    )>,
) {
    let (mut trans, proj, _) = cam.single_mut().unwrap();
    if let (Some(pos), Some(win)) = (mouse.position, mouse.window_size) {
        let margin = 50.;
        let mut delta = Vec3::new(0., 0., 0.);

        if pos.x < margin {
            delta.x = pos.x - margin;
        }
        if pos.x > win.x - margin {
            delta.x = pos.x - win.x + margin;
        }
        if pos.y < margin {
            delta.y = pos.y - margin;
        }
        if pos.y > win.y - margin {
            delta.y = pos.y - win.y + margin;
        }

        trans.translation += delta / margin * proj.scale * 2.0 * time.delta_seconds();
    }
}

fn track_mouse_position(
    mut mouse_motion: EventReader<CursorMoved>,
    mut mouse_leave: EventReader<CursorLeft>,
    mut data: ResMut<MousePosition>,
    windows: Res<window::Windows>,
    camera: Query<(&GlobalTransform, &camera::Camera)>,
) {
    for event in mouse_motion.iter() {
        if event.id == data.window_id {
            data.position = Some(event.position);
        }
    }

    for event in mouse_leave.iter() {
        if event.id == data.window_id {
            data.position = None;
        }
    }

    if let Some(window) = windows.get(data.window_id) {
        data.window_size = Some(Vec2::new(window.width(), window.height()));
    } else {
        data.window_size = None;
    }

    if let (Some(position), Ok((transform, camera)), Some(window_size)) =
        (data.position, camera.single(), data.window_size)
    {
        let ndc_to_world: Mat4 = transform.compute_matrix() * camera.projection_matrix.inverse();
        let norm_pos = position / window_size;
        let pos = ndc_to_world.transform_point3(Vec3::new(
            norm_pos.x * 2. - 1.,
            norm_pos.y * 2. - 1.,
            0.,
        ));
        data.world_position = Some(pos);
        data.tile_position = Some(Vector2::new(
            (pos.x + 0.5).floor() as i32,
            (pos.y + 0.5).floor() as i32,
        ));
    } else {
        data.world_position = None;
        data.tile_position = None;
    }
}

fn block_egui_clicks(ctx: ResMut<EguiContext>, mut clicks: ResMut<Input<MouseButton>>) {
    let ctx = match ctx.try_ctx_for_window(window::WindowId::primary()) {
        Some(c) => c,
        None => return,
    };

    if ctx.is_pointer_over_area() {
        clicks.reset(MouseButton::Left);
        clicks.reset(MouseButton::Right);
    }
}

fn main() -> Result<(), anyhow::Error> {
    jam_common::pretty_env_logger::init();

    App::build()
        .insert_resource(argh::from_env::<JamClient>())
        .insert_resource(WindowDescriptor {
            title: "Jam TD".to_owned(),
            ..Default::default()
        })
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(MousePosition::default())
        .add_plugins(DefaultPlugins)
        .add_plugin(EguiPlugin)
        // Modules
        .add_plugin(enemy::EnemyPlugin)
        .add_plugin(map::MapPlugin)
        .add_plugin(grid::GridPlugin)
        .add_plugin(selector::SelectorPlugin)
        .add_plugin(tower::TowerPlugin)
        .add_plugin(game::GamePlugin)
        // Networking
        .enable_recv(CompHook::<Assembly>::UNRELIABLE)
        .enable_recv(ResHook::<PlayerHealth>::UNRELIABLE)
        .add_plugin(NetPlugin::start("0.0.0.0:0").unwrap())
        // Stuff
        .init_resource::<Pallet>()
        .add_startup_system(setup.system())
        .add_system(block_egui_clicks.system().label("block_clicks"))
        .add_system(track_mouse_position.system())
        .add_system(slide_camera.system())
        .add_system(zoom_camera.system())
        .add_system(reload_server.system())
        .add_system(bevy::input::system::exit_on_esc_system.system())
        .run();

    Ok(())
}
