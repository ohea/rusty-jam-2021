use bevy::prelude::*;
use bevy_egui::{egui, EguiContext};
use jam_common::game::{Game, GameState, PlayerState};
use jam_common::net::{prelude::*, NetStatus};

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut AppBuilder) {
        if let Some(name) = app
            .world()
            .get_resource::<crate::JamClient>()
            .unwrap()
            .name
            .clone()
        {
            app.insert_resource(MyUserId(name));
        }

        app.init_resource::<PlayerState>();
        app.enable_send(ResHook::<PlayerState>::RELIABLE);
        app.enable_recv(ResHook::<Game>::RELIABLE);
        app.add_system(ready_up.system());
        app.add_system(show_game_state.system());
    }
}

fn ready_up(keys: Res<Input<KeyCode>>, game: Res<Game>, mut state: ResMut<PlayerState>) {
    if keys.just_pressed(KeyCode::C) {
        state.ready_for = game.wave_num + 1;
    }
}

fn show_game_state(ctx: ResMut<EguiContext>, game: Res<Game>, net: Res<NetStatus>) {
    egui::Window::new("Game Info").show(ctx.ctx(), |ui| {
        if net.peers == 0 {
            ui.label("Game not running!");
            ui.label("Start \"jam-server\" and reconnect by pressing \"R\".");
            return;
        }

        match game.state {
            GameState::Before => {
                ui.label(format!("Press \"C\" to start game."));
                for (user_id, ready_state) in game.ready_state.iter() {
                    let state = match ready_state {
                        true => "Ready!",
                        false => "Not ready!",
                    };
                    ui.label(format!("{}: {}", user_id, state));
                }
            }
            GameState::Waiting => {
                if let Some(count) = game.countdown {
                    ui.label(format!("Wave {} starting in {}s.", game.wave_num, count));
                }
                ui.label(format!(
                    "Press \"C\" to start wave now ({}/{}).",
                    game.ready_state.iter().filter(|&(_, &s)| s).count(),
                    game.ready_state.len()
                ));
            }
            GameState::Playing => {
                ui.label(format!("Round Active: Wave {}", game.wave_num));
            }
        }
    });
}
